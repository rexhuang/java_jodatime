/*
專案需引入joda-time lib  (http://www.joda.org/joda-time/ )

可以使用http://www.timeanddate.com/worldclock/converter.html 測試橫跨日光節約時間

日光節約時間起迄日請參考http://www.timeanddate.com/time/change/

也可參考kojilin的投影片 
http://www.slideshare.net/kojilin/java8-time?ref=http://www.slideee.com/slide/java8-time

從JDK時間API演進，看時間處理
http://www.ithome.com.tw/node/80076

 */

import java.text.SimpleDateFormat;
import java.util.TimeZone;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.DateTime;

public class sqlDatetimeToLocaltime {
	public static void main(String[] args) throws Exception{
		
		
	     String sqlserverDateTime = "2014-07-14 21:03:45.500";
		//final String serverTimeZone = "+08:00"; //ISO 8601 time zone,Java 6不能使用
		final String serverTimeZone = "+0800"; //RFC 822 time zone
		final String localTimeZone = "PST"; // 不需用PDT
		
		if(sqlserverDateTime.length() == 21){
			sqlserverDateTime = sqlserverDateTime+"00";
		}else if( sqlserverDateTime.length() == 22){
			sqlserverDateTime = sqlserverDateTime+"0";
		}
		
		if(sqlserverDateTime.length() == 23){
			
			/* Java 1.6 doesn't support ISO 8601 time zone
			 * http://stackoverflow.com/questions/3914404/how-to-get-current-moment-in-iso-8601-format
			 * http://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
			 * X - is used for ISO 8601 time zone
			String javaDateTime = sqlserverDateTime.substring(0, 10)+"T"+sqlserverDateTime.substring(11, 23)+serverTimeZone;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			 */
			
			/* Java 6需用RFC 822 time zone*/
			String javaDateTime = sqlserverDateTime.substring(0, 10)+"T"+sqlserverDateTime.substring(11, 23)+serverTimeZone;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");			
			
			
			/*犧牲掉SSS毫秒
			String javaDateTime = sqlserverDateTime.substring(0, 10)+"T"+sqlserverDateTime.substring(11, 19);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			*/
			
			DateTime dtServer = new DateTime(sdf.parse(javaDateTime));
			TimeZone tzLocal = TimeZone.getDefault();
			tzLocal.setID(localTimeZone); 
			DateTime dtLocal = dtServer.withZone(DateTimeZone
					.forTimeZone(tzLocal));
			
			System.out.println("Server Time:"+sqlserverDateTime);
			System.out.println("Local  Time:"+dtLocal);
			
			/*  看需不需要DateTimeFormatter
			DateTimeFormatter outputFormatter = DateTimeFormat.forPattern(
					"yyyy-MM-dd H:mm:ss").withZone(
					DateTimeZone.forID("America/Los_Angeles"));
			System.out.println(outputFormatter.print(dtLocal) + " Timezone:"
					+ dtLocal.getZone());
		  */


		}else{
			throw new Exception("sqlserverDateTime length not correct");
		}
			
	}
	
}
