//http://www.tutorialspoint.com/java/util/timezone_setid.htm

import java.util.*;

public class TimeZoneDemo {

	public static void main(String[] args) {
	      // create time zone object 
	      TimeZone tzone = TimeZone.getDefault();
	      
	      // set time zone ID
	      tzone.setID("GMT+08:00");

	      // checking time zone ID
	      System.out.println("Time zone ID:" +tzone.getID());

	}

}
