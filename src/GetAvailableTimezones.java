//http://obscuredclarity.blogspot.tw/2010/08/get-all-timezones-available-in-timezone.html



import java.util.Arrays;
import java.util.TimeZone;

public class GetAvailableTimezones {

	public static void main(String[] args) {
		String[] allTimeZones = TimeZone.getAvailableIDs();
		Arrays.sort(allTimeZones);
		
		for (int i = 0; i < allTimeZones.length; i++) {
			System.out.println(allTimeZones[i]);
		}

	}

}
